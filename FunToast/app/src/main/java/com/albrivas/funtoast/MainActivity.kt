package com.albrivas.funtoast

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toast("Bienvenido a DevExperto")
        toastDifferent("Bienvenido a DevExperto")
        //changeText("Hola DevExperto")
        acciones()
    }

    private fun acciones() {
        button.setOnClickListener(this)
    }

    //TODO: Crear una funcion llamada Toast que muestre un mensaje al iniciar la aplicacion

    fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    //TODO: Cambiar el mensage de un TexView
    /*fun changeText(text: String) {
        textView.text = text
    }*/

    private fun toastDifferent(message: String)  = Toast.makeText(this, message, Toast.LENGTH_LONG).show()

    //TODO: usar string templates para obtener un texto y mostrarlo pulsando un boton
    override fun onClick(v: View?) {
        toast("Bienvenido ${editext.text}, acabas de mostrar un mensaje")
    }
}
